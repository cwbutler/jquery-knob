// Function called when knob is rotated.
function changeInput (element) {
  var knob = element.data('knob');
  $(knob.inputSelector).val(knob.value);
}

// Initialize all knobs
$('.knob').knobs({
  knobSpriteUrl: '/images/knobs.png',
  knobWidth: 66,
  knobHeight: 66,
  minKnobValue: 0,
  maxKnobValue: 126,
  mouseSensitivity: 1,
  afterRotate: changeInput
});

// Set knobs individually
$('#knob1').knobs('setValue', 0);
$('#knob2').knobs('setValue', 31);
$('#knob3').knobs('setValue', 63);
$('#knob4').knobs('setValue', 96);
$('#knob5').knobs('setValue', 126);

// Get knob values individually
console.log('Knob #1 Value:', $('#knob1').knobs('getValue'));
console.log('Knob #2 Value:', $('#knob2').knobs('getValue'));
console.log('Knob #3 Value:', $('#knob3').knobs('getValue'));
console.log('Knob #4 Value:', $('#knob4').knobs('getValue'));
console.log('Knob #5 Value:', $('#knob5').knobs('getValue'));