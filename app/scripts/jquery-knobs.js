(function ( $ ) {
  'use strict';

  /**
   * [knobs description]
   * @param  {[type]} options [description]
   * @return {[type]}         [description]
   */
  $.fn.knobs = function( options, argvalue ) {

    if (typeof options === 'string' && $.fn.knobs[options]) {
      if (options.indexOf('get') === -1) {
        return this.each(function () {
          $.fn.knobs[options]($(this), argvalue);
        });
      } else { return $.fn.knobs[options](this.eq(0)); }
    }

    // Knob settings
    var settings = $.extend($.fn.knobs.defaults, options);

    return this.each(function() {
      var element = $(this);
      var knob = element.data('knob') || element.data('knob', {}).data('knob');

      // Set knob settings and initialize.
      knob.settings = settings;
      $.fn.knobs.init(element);

      // Get knob position on mouseover
      element.on('mouseover', function (event) {
        knob.lastMouseX = event.pageX;
        knob.lastMouseY = event.pageY;
        knob.position = $.fn.knobs.getPosition(element);
        knob.value = $.fn.knobs.getValue(element);
      });

      // Reacts to mouse wheel movement.
      element.mousewheel(function(event) {
        event.preventDefault();
        // Increment or decrement based off delta.
        if (event.deltaY > 0) { $.fn.knobs.increment(element); }
        if (event.deltaY < 0) { $.fn.knobs.decrement(element); }
      });

      // Reacts to 'mousedown' on a knob.
      element.on('mousedown', function (event) {
        event.preventDefault();
        knob.isDragging = true;
      });

      // Binds to 'mousemove' event.
      $(document).bind('mousemove', function (event) {
        if(knob.isDragging) {
          // Calculate delta based off last mouse move.
          var deltaX = event.pageX - knob.lastMouseX;
          var deltaY = event.pageY - knob.lastMouseY;

          // Save mouse movement for future calculations.
          knob.lastMouseX = event.pageX;
          knob.lastMouseY = event.pageY;

          // Increment/Decrement based off delta.
          if(deltaY < 0) { $.fn.knobs.increment(element, -deltaY); }
          if(deltaY > 0) { $.fn.knobs.decrement(element, deltaY); }
        }
      });

      // Binds to 'mouseup' event.
      $(document).bind('mouseup', function (event) {
        event.preventDefault();
        // Reset dragging flag.
        knob.isDragging = false;
      });

      $(knob.inputSelector).change(function () {
        $.fn.knobs.setValue(element, parseInt($(this).val()));
      });

      if($(knob.inputSelector).tooltip) {
        $(knob.inputSelector).tooltip({title: 'Change "' + knob.name + '" value', placement: 'bottom'});
      }

      if(element.tooltip) {
        element.tooltip({
          title: 'Click and drag in the up or down direction. Or use mouse wheel!',
          placement: 'top',
        });
      }
    });
  };

  // Knob default values and callbacks.
  $.fn.knobs.defaults = {
    knobWidth: 66,
    knobHeight: 66,
    minKnobValue: 0,
    maxKnobValue: 127,
    mouseSensitivity: 0,
    knobImageUrl: 'images/knobs.png',
    beforeInit: function () {},
    beforeRotate: function () {},
    afterRotate: function () {},
    afterInit: function () {},
    beforeIncrement: function () {},
    afterIncrement: function () {},
    beforeDecrement: function () {},
    afterDecrement: function () {},
    defaultKnobValue: function () { return 0; }
  };

  /**
   * Initializes a knob object.
   * @param  {[type]} element The html element that the knob is defined on.
   */
  $.fn.knobs.init = function (element) {
    var knob = element.data('knob');
    // Calls 'beforeInit' callback.
    knob.settings.beforeInit(element);

    // Define some css.
    element.css('background', 'transparent url("' + knob.settings.knobSpriteUrl + '") no-repeat scroll');
    element.css('width', knob.settings.knobWidth);
    element.css('height', knob.settings.knobWidth);

    // Init knob object.
    knob.element = element;
    knob.name = element[0].id;
    knob.inputSelector = '#' + knob.name + '-input';
    knob.position = $.fn.knobs.getPosition(element);
    knob.value = $.fn.knobs.getValue(element);

    if(knob.settings.mouseSensitivity < 0) { knob.settings.mouseSensitivity = 0; }

    $.fn.knobs.setValue(element, knob.settings.defaultKnobValue());

    // Calls 'afterInit' callback.
    knob.settings.afterInit(element);
  };

  /**
   * [getPosition description]
   * @param  {Object} element   The html element that the knob is defined on.
   * @return {Object} position  Returns a position object with
   *
   * Ex: {cssPosition: '0px 0px', x: '0', y: '0'}
   */
  $.fn.knobs.getPosition = function (element) {
    var position = {};

    // Get the x and y pixels of the sprite image.
    position.cssPosition = element.css('background-position').split(' ');
    // Convert the x and y pixels to numbers.
    position.x = parseInt(position.cssPosition[0].slice(0, -2));
    position.y = parseInt(position.cssPosition[1].slice(0, -2));

    return position;
  };

  /**
   * Returns the value of a knob element.
   * @param  {Object} element The html element that the knob is defined on.
   * @return {Number}         The value of the knob.
   */
  $.fn.knobs.getValue = function (element) {
    var knob = element.data('knob');
    return knob.value;
  };

  /**
   * Set the value of a knob.
   * @param {Object} element The html element that the knob is defined on.
   * @param {Number} value   The value to store to the knob.
   */
  $.fn.knobs.setValue = function (element, value) {
    var knob = element.data('knob');

    // Sets value only if within defined boundaries.
    if(value <= knob.settings.maxKnobValue && value >= knob.settings.minKnobValue) {
      knob.value = value;
      $.fn.knobs.rotate(element);
    }
  };

  /**
   * Rotate a knob based off direction value.
   * @param  {Object} element   The html element that the knob is defined on.
   * @param  {Number} direction A negative or positve number that represents which direction to rotate.
   */
  $.fn.knobs.rotate = function (element) {
    var knob = element.data('knob');
    // Call 'beforeRotate' callback
    knob.settings.beforeRotate(element);

    // Calculate and move sprite image position based off knob value.
    knob.position.x = knob.value * -knob.settings.knobWidth;
    element.css('background-position', knob.position.x + 'px 0px');

    // Call 'onRotate' callback
    knob.settings.afterRotate(element);
  };

  /**
   * Decrements a knob value by 1
   * @param  {Object} element [description]
   */
  $.fn.knobs.increment = function (element, sensitivity) {
    var knob = element.data('knob');
    var value = $.fn.knobs.getValue(element) + 1;
    // Call 'beforeIncrement' callback;
    knob.settings.beforeIncrement(element);

    if(sensitivity) { value = value + sensitivity }
    $.fn.knobs.setValue(element, value);

    // Call 'onIncrement' callback;
    knob.settings.afterIncrement(element);
  };

  /**
   * Decrements a knob value by 1
   * @param  {Object} element [description]
   */
  $.fn.knobs.decrement = function (element, sensitivity) {
    var knob = element.data('knob');
    var value = $.fn.knobs.getValue(element) - 1;
    // Call 'beforeDecrement' callback;
    knob.settings.beforeDecrement(element);

    if(sensitivity) { value = value - sensitivity }
    // Call 'afterDecrement' callback;
    $.fn.knobs.setValue(element, value);

    // Call 'afterDecrement' callback;
    knob.settings.afterDecrement(element);
  };

}( jQuery ));
